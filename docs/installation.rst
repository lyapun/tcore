============
Installation
============

At the command line::

    $ easy_install core

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv core
    $ pip install core
