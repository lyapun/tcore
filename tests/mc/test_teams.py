import unittest

from tcore.mc import teams


class TestGroupTeamByGroups(unittest.TestCase):
    def test_two_groups_eight_teams(self):
        _teams = [
            {'id': 1, 'group_id': 1},
            {'id': 2, 'group_id': 2},
            {'id': 3, 'group_id': 1},
            {'id': 4, 'group_id': 2},
            {'id': 5, 'group_id': 1},
            {'id': 6, 'group_id': 2},
            {'id': 7, 'group_id': 1},
            {'id': 8, 'group_id': 2},
        ]
        by_groups = teams.group_team_by_groups(_teams)
        self.assertEqual(
            by_groups,
            {
                1: [
                    {'id': 1, 'group_id': 1},
                    {'id': 3, 'group_id': 1},
                    {'id': 5, 'group_id': 1},
                    {'id': 7, 'group_id': 1},
                ],
                2: [
                    {'id': 2, 'group_id': 2},
                    {'id': 4, 'group_id': 2},
                    {'id': 6, 'group_id': 2},
                    {'id': 8, 'group_id': 2},
                ]
            }
        )

    def test_one_group_four_teams(self):
        _teams = [
            {'id': 1, 'group_id': 1},
            {'id': 2, 'group_id': 1},
            {'id': 3, 'group_id': 1},
            {'id': 4, 'group_id': 1},
        ]
        by_groups = teams.group_team_by_groups(_teams)
        self.assertEqual(
            by_groups,
            {
                1: [
                    {'id': 1, 'group_id': 1},
                    {'id': 2, 'group_id': 1},
                    {'id': 3, 'group_id': 1},
                    {'id': 4, 'group_id': 1},
                ],
            }
        )
