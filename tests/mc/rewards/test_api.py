#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
mc.test_rewards
----------------------------------

Tests for `mc.rewards` module.
"""

import unittest

from tcore.mc import rewards


class TestIsMCFitRequirements(unittest.TestCase):
    def test_true(self):
        game_state = {
            'history': [1, 2, 3, 4, 5]
        }
        self.assertTrue(rewards.is_mc_fit_requirements(game_state))

    def test_false(self):
        game_state = {
            'history': [1, 2, 3, 4]
        }
        self.assertFalse(rewards.is_mc_fit_requirements(game_state))


class TestGetRewardsMessage(unittest.TestCase):
    def test_not_fit_requirements(self):
        message = rewards.get_rewards_message(False, 1, 1)
        self.assertEqual(
            message,
            'Not enough games were recorded in this session. Therefore, the performance points will not be given out.'
        )

    def test_first_place(self):
        message = rewards.get_rewards_message(True, 1, 1)
        self.assertEqual(
            message,
            'Awesome! You were the best team today!'
        )

    def test_second_place(self):
        message = rewards.get_rewards_message(True, 2, 3)
        self.assertEqual(
            message,
            'Almost made it...'
        )


class TestCalculateTeamPoints(unittest.TestCase):
    teams = [
        {'name': 'A', 'players': [22, 40, 39, 41, 56], 'id': 1},
        {'name': 'B', 'players': [27, 26, 13, 11, 168], 'id': 2},
        {'name': 'C', 'players': [42, 169, 4, 29, 9], 'id': 3},
        {'name': 'D', 'players': [10, 51, 163, 62, 154], 'id': 4}
    ]
    history = [
        {'team_1': 1, 'team_1_score': 3, 'team_2_score': 0, 'team_2': 2},
        {'team_1': 1, 'team_1_score': 0, 'team_2_score': 3, 'team_2': 3},
        {'team_1': 3, 'team_1_score': 3, 'team_2_score': 3, 'team_2': 4},
        {'team_1': 2, 'team_1_score': 3, 'team_2_score': 2, 'team_2': 1},
        {'team_1': 2, 'team_1_score': 3, 'team_2_score': 3, 'team_2': 1},
        {'team_1': 3, 'team_1_score': 4, 'team_2_score': 3, 'team_2': 4},
    ]

    def test_should_return_zero_points_for_empty_history(self):
        self.assertEqual(
            rewards.calculate_team_points([], self.teams, True),
            [
                {'d': 0, 'g': 0, 'p': 0, 'l': 0, 'w': 0, 'pts': 0, 'plc': 1, 'tid': 1, 'tname': 'A', 'tmp': 1, 'color': None, 'tcolor': None, 'gd': 0, 'gid': 1},
                {'d': 0, 'g': 0, 'p': 0, 'l': 0, 'w': 0, 'pts': 0, 'plc': 2, 'tid': 2, 'tname': 'B', 'tmp': 1, 'color': None, 'tcolor': None, 'gd': 0, 'gid': 1},
                {'d': 0, 'g': 0, 'p': 0, 'l': 0, 'w': 0, 'pts': 0, 'plc': 3, 'tid': 3, 'tname': 'C', 'tmp': 1, 'color': None, 'tcolor': None, 'gd': 0, 'gid': 1},
                {'d': 0, 'g': 0, 'p': 0, 'l': 0, 'w': 0, 'pts': 0, 'plc': 4, 'tid': 4, 'tname': 'D', 'tmp': 1, 'color': None, 'tcolor': None, 'gd': 0, 'gid': 1}
            ]
        )

    def test_should_return_table_for_history(self):
        self.assertEqual(
            rewards.calculate_team_points(self.history, self.teams, True),
            [
                {'l': 0, 'plc': 1, 'p': 3, 'tid': 3, 'g': 4, 'd': 1, 'pts': 7, 'w': 2, 'tname': 'C', 'tmp': 7, 'color': None, 'tcolor': None, 'gd': 4, 'gid': 1},
                {'l': 2, 'plc': 2, 'p': 4, 'tid': 1, 'g': -1, 'd': 1, 'pts': 4, 'w': 1, 'tname': 'A', 'tmp': 4, 'color': None, 'tcolor': None, 'gd': -1, 'gid': 1},
                {'l': 1, 'plc': 3, 'p': 3, 'tid': 2, 'g': -2, 'd': 1, 'pts': 4, 'w': 1, 'tname': 'B', 'tmp': 2, 'color': None, 'tcolor': None, 'gd': -2, 'gid': 1},
                {'l': 1, 'plc': 4, 'p': 2, 'tid': 4, 'g': -1, 'd': 1, 'pts': 1, 'w': 0, 'tname': 'D', 'tmp': 1, 'color': None, 'tcolor': None, 'gd': -1, 'gid': 1}
            ]
        )

    def test_should_return_empty_table_if_game_not_started(self):
        self.assertEqual(
            rewards.calculate_team_points([], self.teams, False),
            [
                {'tid': 1, 'w': 0, 'p': 0, 'pts': 0, 'l': 0, 'g': 0, 'plc': 1, 'd': 0, 'tname': 'A', 'tmp': 0, 'color': None, 'tcolor': None, 'gd': 0, 'gid': 1},
                {'tid': 2, 'w': 0, 'p': 0, 'pts': 0, 'l': 0, 'g': 0, 'plc': 2, 'd': 0, 'tname': 'B', 'tmp': 0, 'color': None, 'tcolor': None, 'gd': 0, 'gid': 1},
                {'tid': 3, 'w': 0, 'p': 0, 'pts': 0, 'l': 0, 'g': 0, 'plc': 3, 'd': 0, 'tname': 'C', 'tmp': 0, 'color': None, 'tcolor': None, 'gd': 0, 'gid': 1},
                {'tid': 4, 'w': 0, 'p': 0, 'pts': 0, 'l': 0, 'g': 0, 'plc': 4, 'd': 0, 'tname': 'D', 'tmp': 0, 'color': None, 'tcolor': None, 'gd': 0, 'gid': 1}
            ]
        )

    def test_case_when_one_team_doesnt_play(self):
        history = [
            {'team_1': 3, 'team_2': 4, 'team_1_score': 0, 'team_2_score': 3},
            {'team_1': 4, 'team_2': 1, 'team_1_score': 0, 'team_2_score': 3},
        ]
        teams = [
            {'name': 'A', 'id': 1},
            {'name': 'B', 'id': 2},
            {'name': 'C', 'id': 3},
            {'name': 'D', 'id': 4}
        ]
        result = list(map(
            lambda obj: obj['tname'],
            rewards.calculate_team_points(history, teams, True)
        ))
        self.assertEqual(result,['A', 'D', 'C', 'B'])


class TestCalculatePlayoffPoints(unittest.TestCase):
    teams = [
        {'name': 'A', 'players': [22, 40, 39, 41, 56], 'id': 1},
        {'name': 'B', 'players': [27, 26, 13, 11, 168], 'id': 2},
        {'name': 'C', 'players': [42, 169, 4, 29, 9], 'id': 3},
        {'name': 'D', 'players': [10, 51, 163, 62, 154], 'id': 4},
        {'name': 'D', 'players': [510, 551, 5163, 562, 5154], 'id': 5},
        {'name': 'D', 'players': [610, 651, 1663, 662, 6154], 'id': 6},
        {'name': 'D', 'players': [710, 751, 7163, 762, 7154], 'id': 7},
        {'name': 'D', 'players': [810, 851, 8163, 862, 8154], 'id': 8},
    ]
    history = [
        {'team_1': 1, 'team_1_score': 3, 'team_2_score': 0, 'team_2': 2},
        {'team_1': 1, 'team_1_score': 0, 'team_2_score': 3, 'team_2': 3},
        {'team_1': 3, 'team_1_score': 3, 'team_2_score': 3, 'team_2': 4},
        {'team_1': 2, 'team_1_score': 3, 'team_2_score': 2, 'team_2': 1},
        {'team_1': 2, 'team_1_score': 3, 'team_2_score': 3, 'team_2': 1},
        {'team_1': 3, 'team_1_score': 4, 'team_2_score': 3, 'team_2': 4},
    ]

    def test_no_history(self):
        self.assertIsNone(rewards.calculate_playoffs_points([], []))

    def test_only_final(self):
        history = [
            {'team_1': 1, 'team_1_score': 3, 'team_2': 2, 'team_2_score': 0, 'stage': 'final'}
        ]
        table = rewards.calculate_playoffs_points(history, self.teams)
        self.assertEqual(
            table,
            [
                {'tmp': 10, 'color': None, 'tname': 'A', 'plc': 1, 'tcolor': None, 'tid': 1},
                {'tmp': 7, 'color': None, 'tname': 'B', 'plc': 2, 'tcolor': None, 'tid': 2}
            ]
        )

    def test_semifinal(self):
        history = [
            {'team_1': 1, 'team_1_score': 3, 'team_2': 4, 'team_2_score': 0, 'stage': 'semi-finals'},
            {'team_1': 2, 'team_1_score': 3, 'team_2': 3, 'team_2_score': 0, 'stage': 'semi-finals'},
            {'team_1': 1, 'team_1_score': 3, 'team_2': 2, 'team_2_score': 0, 'stage': 'final'},
        ]
        table = rewards.calculate_playoffs_points(history, self.teams)
        self.assertEqual(
            table,
            [
                {'tmp': 10, 'color': None, 'tname': 'A', 'plc': 1, 'tcolor': None, 'tid': 1},
                {'tmp': 7, 'color': None, 'tname': 'B', 'plc': 2, 'tcolor': None, 'tid': 2},
                {'tmp': 5, 'color': None, 'tname': 'C', 'plc': 3, 'tcolor': None, 'tid': 3},
                {'tmp': 5, 'color': None, 'tname': 'D', 'plc': 3, 'tcolor': None, 'tid': 4}
            ]
        )

    def test_quarter_final(self):
        history = [
            {'team_1': 1, 'team_1_score': 3, 'team_2': 5, 'team_2_score': 0, 'stage': 'quarter-finals'},
            {'team_1': 2, 'team_1_score': 3, 'team_2': 6, 'team_2_score': 0, 'stage': 'quarter-finals'},
            {'team_1': 3, 'team_1_score': 3, 'team_2': 7, 'team_2_score': 0, 'stage': 'quarter-finals'},
            {'team_1': 4, 'team_1_score': 3, 'team_2': 8, 'team_2_score': 0, 'stage': 'quarter-finals'},
            {'team_1': 1, 'team_1_score': 3, 'team_2': 4, 'team_2_score': 0, 'stage': 'semi-finals'},
            {'team_1': 2, 'team_1_score': 3, 'team_2': 3, 'team_2_score': 0, 'stage': 'semi-finals'},
            {'team_1': 1, 'team_1_score': 3, 'team_2': 2, 'team_2_score': 0, 'stage': 'final'},
        ]
        table = rewards.calculate_playoffs_points(history, self.teams)
        print('table', table)
        self.assertEqual(
            table,
            [
                {'tmp': 10, 'color': None, 'tname': 'A', 'plc': 1, 'tcolor': None, 'tid': 1},
                {'tmp': 7, 'color': None, 'tname': 'B', 'plc': 2, 'tcolor': None, 'tid': 2},
                {'tmp': 5, 'color': None, 'tname': 'C', 'plc': 3, 'tcolor': None, 'tid': 3},
                {'tmp': 5, 'color': None, 'tname': 'D', 'plc': 3, 'tcolor': None, 'tid': 4},
                {'tmp': 3, 'color': None, 'tname': 'D', 'plc': 5, 'tcolor': None, 'tid': 5},
                {'tmp': 3, 'color': None, 'tname': 'D', 'plc': 5, 'tcolor': None, 'tid': 6},
                {'tmp': 3, 'color': None, 'tname': 'D', 'plc': 5, 'tcolor': None, 'tid': 7},
                {'tmp': 3, 'color': None, 'tname': 'D', 'plc': 5, 'tcolor': None, 'tid': 8}
            ]
        )

    def test_quarter_final_with_3rd_place(self):
        history = [
            {'team_1': 1, 'team_1_score': 3, 'team_2': 5, 'team_2_score': 0, 'stage': 'quarter-finals'},
            {'team_1': 2, 'team_1_score': 3, 'team_2': 6, 'team_2_score': 0, 'stage': 'quarter-finals'},
            {'team_1': 3, 'team_1_score': 3, 'team_2': 7, 'team_2_score': 0, 'stage': 'quarter-finals'},
            {'team_1': 4, 'team_1_score': 3, 'team_2': 8, 'team_2_score': 0, 'stage': 'quarter-finals'},
            {'team_1': 1, 'team_1_score': 3, 'team_2': 4, 'team_2_score': 0, 'stage': 'semi-finals'},
            {'team_1': 2, 'team_1_score': 3, 'team_2': 3, 'team_2_score': 0, 'stage': 'semi-finals'},
            {'team_1': 3, 'team_1_score': 3, 'team_2': 4, 'team_2_score': 0, 'stage': '3rd-place'},
            {'team_1': 1, 'team_1_score': 3, 'team_2': 2, 'team_2_score': 0, 'stage': 'final'},
        ]
        table = rewards.calculate_playoffs_points(history, self.teams)
        self.assertEqual(
            table,
            [
                {'tmp': 10, 'color': None, 'tname': 'A', 'plc': 1, 'tcolor': None, 'tid': 1},
                {'tmp': 7, 'color': None, 'tname': 'B', 'plc': 2, 'tcolor': None, 'tid': 2},
                {'tmp': 5, 'color': None, 'tname': 'C', 'plc': 3, 'tcolor': None, 'tid': 3},
                {'tmp': 4, 'color': None, 'tname': 'D', 'plc': 4, 'tcolor': None, 'tid': 4},
                {'tmp': 3, 'color': None, 'tname': 'D', 'plc': 5, 'tcolor': None, 'tid': 5},
                {'tmp': 3, 'color': None, 'tname': 'D', 'plc': 5, 'tcolor': None, 'tid': 6},
                {'tmp': 3, 'color': None, 'tname': 'D', 'plc': 5, 'tcolor': None, 'tid': 7},
                {'tmp': 3, 'color': None, 'tname': 'D', 'plc': 5, 'tcolor': None, 'tid': 8}
            ]

        )

    def test_plc_none_bug(self):
        history = [
            {'team_1': 1, 'team_1_score': 1, 'team_2_score': 0, 'team_2': 8, 'stage': 'quarter-finals'},
        ]
        table = rewards.calculate_playoffs_points(history, self.teams)
        for team in table:
            self.assertIsNotNone(team['plc'])


if __name__ == '__main__':
    unittest.main()
