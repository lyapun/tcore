def group_team_by_groups(teams):
    '''
    Return teams grouped by groups
    :param teams: list of teams
    :return: dict where keys are group ids, and values - list of teams
    '''
    teams_by_groups = {}
    for team in teams:
        group_id = team.get('group_id', 1)
        if group_id not in teams_by_groups:
            teams_by_groups[group_id] = []
        teams_by_groups[group_id].append(team)
    return teams_by_groups
