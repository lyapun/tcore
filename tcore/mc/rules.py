def expand_rules(rules):
    '''
    Transform rules from API format to format more useful to use.
    '''
    if 'number_of_players' not in rules:
        return rules
    new_players = {}
    for players, rule in rules['number_of_players'].items():
        for player in range(int(players.split('-')[0]), int(players.split('-')[1]) + 1):
            new_players[player] = rule
    rules['number_of_players'] = new_players
    return rules
