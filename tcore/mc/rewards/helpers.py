from functools import partial
from itertools import combinations


def group_history_by_groups(history):
    '''
    Groups games by groups
    :param history: list of games
    :return: dict where keys are group ids, and values - list of games
    '''
    history_groups = {}
    for game in history:
        group_id = game.get('group_id', 1)
        if group_id not in history_groups:
            history_groups[group_id] = []
        history_groups[group_id].append(game)
    return history_groups


def _get_teams_table(history, teams):
    from tcore.mc.rewards import get_team_by_id
    table = [
        {
            'tid': team['id'],
            'tname': team.get('name'),
            'color': team.get('color'),
            'tcolor': team.get('tcolor'),
            'plc': None,
            'p': 0,
            'g': 0,
            'pts': 0,
            'tmp': 0,
            'eq': set(),
            'w': 0,   # wins
            'd': 0,   # draws
            'l': 0,   # looses
            'gd': 0,  # goal difference
        } for team in teams
    ]
    for game in history:
        team_1 = game['team_1']
        team_2 = game['team_2']
        team_row1 = get_team_by_id(table, team_1)
        team_row2 = get_team_by_id(table, team_2)
        if not (team_row1 and team_row2):
            continue
        if game['team_1_score'] > game['team_2_score']:
            team_row1['pts'] += 3
            team_row1['w'] += 1
            team_row2['l'] += 1
        elif game['team_1_score'] < game['team_2_score']:
            team_row2['pts'] += 3
            team_row2['w'] += 1
            team_row1['l'] += 1
        else:
            team_row1['pts'] += 1
            team_row2['pts'] += 1
            team_row1['d'] += 1
            team_row2['d'] += 1
        team_row1['g'] += game['team_1_score']
        team_row1['g'] -= game['team_2_score']
        team_row2['g'] += game['team_2_score']
        team_row2['g'] -= game['team_1_score']
        team_row1['p'] += 1
        team_row2['p'] += 1
        team_row1['gd'] += game['team_1_score'] - game['team_2_score']
        team_row2['gd'] += game['team_2_score'] - game['team_1_score']
    return table


def _set_equality(table, history):
    func = partial(_check_equality, history)
    [x for x in map(func, combinations(table, 2))]


def _check_equality(history, first_second):
    first, second = first_second
    result = _compare_teams(history, first, second)
    if result == 0:
        first['eq'].add(second['tid'])
        second['eq'].add(first['tid'])


def _compare_teams(history, first, second):
    if first['p'] == 0 or second['p'] == 0:
        return simple_compare(first['p'], second['p'])
    return (
        compare_points(first['pts'], second['pts']) or
        compare_goals(first['g'], second['g']) or
        compare_head_to_head_points(first['tid'], second['tid'], history) or
        compare_head_to_head_goals(first['tid'], second['tid'], history) or
        compare_games(first['p'], second['p'])
    )


def compare_head_to_head_points(first_id, second_id, history):
    first_points, second_points = get_head_to_head_points(first_id, second_id, history)
    return simple_compare(first_points, second_points)


def get_head_to_head_points(first_id, second_id, history):
    first_points = 0
    second_points = 0
    for game in history:
        if game['team_1'] in (first_id, second_id) and game['team_2'] in (first_id, second_id):
            first_team_score = game['team_1_score'] if game['team_1'] == first_id else game['team_2_score']
            second_team_score = game['team_1_score'] if game['team_1'] == second_id else game['team_2_score']
            if first_team_score > second_team_score:
                first_points += 3
            elif first_team_score < second_team_score:
                second_points += 3
            else:
                first_points += 1
                second_points += 1
    return first_points, second_points


def compare_head_to_head_goals(first_id, second_id, history):
    first_points, second_points = get_head_to_head_goals(first_id, second_id, history)
    return simple_compare(first_points, second_points)


def get_head_to_head_goals(first_id, second_id, history):
    first_goals = 0
    second_goals = 0
    for game in history:
        if game['team_1'] in (first_id, second_id) and game['team_2'] in (first_id, second_id):
            first_goals += game['team_1_score'] if game['team_1'] == first_id else game['team_2_score']
            second_goals += game['team_1_score'] if game['team_1'] == second_id else game['team_2_score']
    return first_goals, second_goals


def simple_compare(first, second):
    if first < second:
        return -1
    elif first > second:
        return 1
    else:
        return 0


def inverse(result):
    if result == -1:
        return 1
    elif result == 1:
        return -1
    else:
        return 0


def simple_compare_inverse(first, second):
    return inverse(simple_compare(first, second))


compare_points = simple_compare
compare_goals = simple_compare
compare_games = simple_compare_inverse


def get_looser_from_game(game):
    return game['team_1'] if game['team_1_score'] < game['team_2_score'] else game['team_2']


def get_winner_from_game(game):
    return game['team_1'] if game['team_1_score'] > game['team_2_score'] else game['team_2']
