import logging

log = logging.getLogger('tcore')


def get_turfmapp_points_for_team_and_place_and_table(team, place, table):
    if len(team['eq']) > 0:
        place += len(team['eq'])
        for another_team in table:
            if team['tid'] in another_team['eq']:
                another_team['eq'].remove(team['tid'])
    return get_turfmapp_points_for_place_and_teams_count(place, len(table))


def get_turfmapp_points_for_platoffs(place):
    return {
        1: 10,
        2: 7,
        3: 5,
        4: 4,
        5: 3,
        6: 3,
        7: 3,
        8: 3,
    }.get(place, 1)


def get_turfmapp_points_for_place_and_teams_count(place, team_count):
    if team_count == 2:
        return {
            1: 4,
            2: 1,
        }[place]
    elif team_count == 3:
        return {
            1: 4,
            2: 2,
            3: 1,
        }[place]
    elif team_count == 4:
        return {
            1: 7,
            2: 4,
            3: 2,
            4: 1,
        }[place]
    elif team_count == 5:
        return {
            1: 7,
            2: 5,
            3: 3,
            4: 2,
            5: 1,
        }[place]
    elif team_count == 6:
        return {
            1: 7,
            2: 5,
            3: 3,
            4: 2,
            5: 1,
            6: 1,
        }[place]
    elif team_count == 7:
        return {
            1: 7,
            2: 5,
            3: 3,
            4: 2,
            5: 1,
            6: 1,
            7: 1,
        }[place]
    elif team_count == 8:
        return {
            1: 7,
            2: 5,
            3: 3,
            4: 2,
            5: 1,
            6: 1,
            7: 1,
            8: 1,
        }[place]
    elif team_count == 9:
        return {
            1: 7,
            2: 5,
            3: 3,
            4: 2,
            5: 2,
            6: 1,
            7: 1,
            8: 1,
            9: 1,
        }[place]
    elif team_count == 10:
        return {
            1: 10,
            2: 7,
            3: 5,
            4: 4,
            5: 3,
            6: 2,
            7: 1,
            8: 1,
            9: 1,
            10: 1,
        }[place]
    elif team_count == 11:
        return {
            1: 10,
            2: 7,
            3: 5,
            4: 4,
            5: 3,
            6: 2,
            7: 2,
            8: 1,
            9: 1,
            10: 1,
            11: 1,
        }[place]
    elif team_count == 12:
        return {
            1: 10,
            2: 7,
            3: 5,
            4: 4,
            5: 3,
            6: 3,
            7: 2,
            8: 2,
            9: 1,
            10: 1,
            11: 1,
            12: 1,
        }[place]
    elif team_count == 13:
        return {
            1: 10,
            2: 7,
            3: 5,
            4: 4,
            5: 3,
            6: 3,
            7: 2,
            8: 2,
            9: 1,
            10: 1,
            11: 1,
            12: 1,
            13: 1,
        }[place]
    elif team_count == 14:
        return {
            1: 10,
            2: 7,
            3: 5,
            4: 4,
            5: 3,
            6: 3,
            7: 2,
            8: 2,
            9: 1,
            10: 1,
            11: 1,
            12: 1,
            13: 1,
            14: 1,
        }[place]
    elif team_count == 15:
        return {
            1: 10,
            2: 7,
            3: 5,
            4: 4,
            5: 3,
            6: 3,
            7: 2,
            8: 2,
            9: 1,
            10: 1,
            11: 1,
            12: 1,
            13: 1,
            14: 1,
            15: 1,
        }[place]
    elif team_count in (16, 17):
        return {
            1: 12,
            2: 8,
            3: 6,
            4: 5,
            5: 4,
            6: 4,
            7: 3,
            8: 3,
            9: 2,
            10: 2,
            11: 1,
            12: 1,
            13: 1,
            14: 1,
            15: 1,
            16: 1,
            17: 1,
        }[place]
    elif team_count in (18, 19):
        return {
            1: 12,
            2: 8,
            3: 6,
            4: 5,
            5: 4,
            6: 4,
            7: 3,
            8: 3,
            9: 2,
            10: 2,
            11: 2,
            12: 1,
            13: 1,
            14: 1,
            15: 1,
            16: 1,
            17: 1,
            18: 1,
            19: 1,
        }[place]
    elif team_count in (20, 21):
        return {
            1: 14,
            2: 10,
            3: 7,
            4: 6,
            5: 5,
            6: 5,
            7: 4,
            8: 4,
            9: 3,
            10: 3,
            11: 2,
            12: 2,
            13: 1,
            14: 1,
            15: 1,
            16: 1,
            17: 1,
            18: 1,
            19: 1,
            20: 1,
            21: 1,
        }[place]
    elif team_count in (22, 23):
        return {
            1: 14,
            2: 10,
            3: 7,
            4: 6,
            5: 5,
            6: 5,
            7: 4,
            8: 4,
            9: 3,
            10: 3,
            11: 2,
            12: 2,
            13: 2,
            14: 1,
            15: 1,
            16: 1,
            17: 1,
            18: 1,
            19: 1,
            20: 1,
            21: 1,
            22: 1,
            23: 1,
        }[place]
    elif team_count in (24, 25):
        return {
            1: 16,
            2: 12,
            3: 9,
            4: 8,
            5: 7,
            6: 7,
            7: 6,
            8: 6,
            9: 5,
            10: 5,
            11: 4,
            12: 4,
            13: 3,
            14: 3,
            15: 2,
            16: 2,
            17: 1,
            18: 1,
            19: 1,
            20: 1,
            21: 1,
            22: 1,
            23: 1,
            24: 1,
            25: 1,
        }[place]
    elif team_count in (26, 27):
        return {
            1: 16,
            2: 12,
            3: 9,
            4: 8,
            5: 7,
            6: 7,
            7: 6,
            8: 6,
            9: 5,
            10: 5,
            11: 4,
            12: 4,
            13: 3,
            14: 3,
            15: 2,
            16: 2,
            17: 2,
            18: 1,
            19: 1,
            20: 1,
            21: 1,
            22: 1,
            23: 1,
            24: 1,
            25: 1,
            26: 1,
            27: 1,
        }[place]
    elif team_count in (28, 29):
        return {
            1: 18,
            2: 14,
            3: 11,
            4: 10,
            5: 9,
            6: 9,
            7: 8,
            8: 8,
            9: 7,
            10: 7,
            11: 6,
            12: 6,
            13: 5,
            14: 5,
            15: 4,
            16: 4,
            17: 3,
            18: 3,
            19: 2,
            20: 2,
            21: 1,
            22: 1,
            23: 1,
            24: 1,
            25: 1,
            26: 1,
            27: 1,
            28: 1,
            29: 1,
        }[place]
    elif team_count in (30, 31):
        return {
            1: 18,
            2: 14,
            3: 11,
            4: 10,
            5: 9,
            6: 9,
            7: 8,
            8: 8,
            9: 7,
            10: 7,
            11: 6,
            12: 6,
            13: 5,
            14: 5,
            15: 4,
            16: 4,
            17: 3,
            18: 3,
            19: 2,
            20: 2,
            21: 2,
            22: 1,
            23: 1,
            24: 1,
            25: 1,
            26: 1,
            27: 1,
            28: 1,
            29: 1,
            30: 1,
            31: 1,
        }[place]
    elif team_count == 32:
        return {
            1: 20,
            2: 15,
            3: 12,
            4: 11,
            5: 10,
            6: 10,
            7: 9,
            8: 9,
            9: 8,
            10: 8,
            11: 7,
            12: 7,
            13: 6,
            14: 6,
            15: 5,
            16: 5,
            17: 4,
            18: 4,
            19: 3,
            20: 3,
            21: 2,
            22: 2,
            23: 2,
            24: 2,
            25: 1,
            26: 1,
            27: 1,
            28: 1,
            29: 1,
            30: 1,
            31: 1,
            32: 1,
        }[place]
    else:
        log.warning('Unsupported amount of teams: %s', team_count)
        return 0
