from functools import cmp_to_key
from functools import partial

from tcore.mc.rewards.helpers import group_history_by_groups, _get_teams_table, _set_equality, _compare_teams, \
    get_looser_from_game, get_winner_from_game
from tcore.mc.rewards.messages import PLACE_MESSAGES
from tcore.mc.rewards.points import get_turfmapp_points_for_team_and_place_and_table, get_turfmapp_points_for_platoffs
from tcore.mc.teams import group_team_by_groups


def is_mc_fit_requirements(game_state):
    '''
    Checks if MC fits minimum requirements in order to give users new points.
    :param game_state: dict representation of MC game state
    :return: bool
    '''
    return (
        game_state and
        len(game_state.get('history', []) + (game_state.get('playoffs_history', []) or [])) >= 5
    )


def get_rewards_message(is_current_mc_fit_requirements, team_plc, teams_count, is_playoffs=False, playoff_plc=None):
    '''
    Returns rewards string for provided team place and teams count

    :param is_current_mc_fit_requirements: (bool)
    :param team_plc: (int)
    :param teams_count: (int)
    :param is_playoffs: (bool)
    :param playoff_plc: (int)
    :return: (str) rewards message
    '''
    if not is_current_mc_fit_requirements:
        return 'Not enough games were recorded in this session. Therefore, the performance points will not be given out.'
    if is_playoffs:
        if playoff_plc is None:
            return 'You were not qualified to playoffs. Good luck next time!'
        elif playoff_plc >= 65:
            return 'You played in 64th finals! Good start, but can be better next time!'
        elif playoff_plc >= 33:
            return 'You played in 32nd finals! Good result, but can be better next time!'
        elif playoff_plc >= 17:
            return 'You played in 16th finals! Not bad, but wish you to play even better next time!'
        elif playoff_plc >= 9:
            return "You played in 8th finals! It's the good result; you can be proud by yourself!"
        elif playoff_plc >= 5:
            return 'Wow, you went to quarter finals! So close!'
        elif playoff_plc == 4:
            return "You were in the semifinal. It's the great result!"
        elif playoff_plc == 3:
            return 'You got the 3-rd place! Congratulations! See you in final next time!'
        elif playoff_plc == 2:
            return 'Second place! Good luck in the final next time!'
        elif playoff_plc == 1:
            return 'First place! You are the best team today! Get your cup and celebrate well!'
    else:
        if team_plc == 1:
            return 'Awesome! You were the best team today!'
        return PLACE_MESSAGES.get(teams_count, {}).get(team_plc, '')


def calculate_team_points(history, teams, is_current_mc_fit_requirements):
    '''
    Returns table for provided history and teams
    :param history: list of games
    :param teams: list of teams
    :param is_current_mc_fit_requirements: (bool)
    :return: list of team results
    '''
    team_groups = group_team_by_groups(teams)
    general_group = []
    history_groups = group_history_by_groups(history)
    for group_id, teams in team_groups.items():
        group_history = history_groups.get(group_id) or []
        table = _get_teams_table(group_history, teams)
        cmp_func = partial(_compare_teams, group_history)
        table = sorted(table, key=cmp_to_key(cmp_func), reverse=True)
        _set_equality(table, group_history)
        for place, team in enumerate(table, 1):
            team['plc'] = place
            team['gid'] = group_id
            team['tmp'] = get_turfmapp_points_for_team_and_place_and_table(team, place, table) if is_current_mc_fit_requirements else 0
        for team in table:
            del team['eq']
        general_group += table
    return general_group


def calculate_playoffs_points(history, teams):
    '''
    Returns table for provided playoffs history and teams
    :param history: list of playoff games
    :param teams: list of teams
    :return: list of team results
    '''
    if not history:
        return None

    playoff_team_ids = set()
    for game in history:
        playoff_team_ids.add(game['team_1'])
        playoff_team_ids.add(game['team_2'])
    table = [
        {
            'tid': team['id'],
            'tname': team.get('name'),
            'color': team.get('color'),
            'tcolor': team.get('tcolor'),
            'plc': None,
            'tmp': None,
        } for team in teams if team['id'] in playoff_team_ids
    ]

    team_places = {}
    for game in history:
        team_loosed = get_looser_from_game(game)
        team_win = get_winner_from_game(game)
        if game['stage'] == '64th finals':
            team_places[team_loosed] = 65
            team_places[team_win] = 64
        if game['stage'] == '32th finals':
            team_places[team_loosed] = 33
            team_places[team_win] = 32
        if game['stage'] == '16th finals':
            team_places[team_loosed] = 17
            team_places[team_win] = 16
        if game['stage'] == '8th finals':
            team_places[team_loosed] = 9
            team_places[team_win] = 8
        elif game['stage'] == 'quarter-finals':
            team_places[team_loosed] = 5
            team_places[team_win] = 4
        elif game['stage'] == 'semi-finals':
            team_places[team_loosed] = 3
            team_places[team_win] = 2
        elif game['stage'] == '3rd-place':
            team_places[team_loosed] = 4
            team_places[team_win] = 3
        elif game['stage'] == 'final':
            team_places[team_loosed] = 2
            team_places[team_win] = 1
    for team in table:
        place = team_places.get(team['tid'])
        team['plc'] = place
        team['tmp'] = get_turfmapp_points_for_platoffs(place) if place else None
    table = sorted(table, key=lambda obj: obj['plc'] or 999)
    return table


def get_team_by_id(table, team_id):
    '''
    Get team by id
    :param table:
    :param team_id:
    :return:
    '''
    teams = [team for team in table if team['tid'] == team_id]
    return teams[0] if len(teams) > 0 else None
