PLACE_MESSAGES = {
    3: {
        2: "Almost made it...",
        3: "Better luck next time!",
    },
    4: {
        2: "Great work!",
        3: "Step it up next time!",
        4: "Oh man...",
    },
    5: {
        2: "Great work!",
        3: "Not too bad!",
        4: "Step it up next time!",
        5: "Oh man...",
    },
    6: {
        2: "Great work!",
        3: "Not too bad!",
        4: "Better luck next time!",
        5: "Step it up next time!",
        6: "Oh man...",
    },
    7: {
        2: "Great work!",
        3: "Not too bad!",
        4: "Just about average",
        5: "Better luck next time!",
        6: "Step it up next time!",
        7: "Oh man...",
    },
    8: {
        2: "Great work!",
        3: "Not too bad!",
        4: "Just about average",
        5: "Could be better",
        6: "Better luck next time!",
        7: "Step it up next time!",
        8: "Oh man...",
    },
    9: {
        2: "Great work!",
        3: "Good work!",
        4: "Not too bad!",
        5: "Just about average",
        6: "Could be better",
        7: "Better luck next time!",
        8: "Step it up next time!",
        9: "Oh man...",
    },
    10: {
        2: "Great work!",
        3: "Good work!",
        4: "Not too bad!",
        5: "Just about average",
        6: "Just below average",
        7: "Could be better",
        8: "Better luck next time!",
        9: "Step it up next time!",
        10: "Oh man...",
    },
}
