import sqlalchemy as sa

metadata = sa.MetaData()


user_table = sa.Table(
    'user', metadata,
    sa.Column('id', sa.String(255)),
    sa.Column('added_id', sa.Integer, primary_key=True),
    sa.Column('first_name', sa.String(255)),
    sa.Column('last_name', sa.String(255)),
    sa.Column('is_staff', sa.Boolean),
)
